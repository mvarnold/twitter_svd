import os
import sys
import datetime
sys.path.insert(0, '/home/mvarnold/1-gram_search/.passwords')
from mongo_password import x as pwd 
sys.path.insert(0, '/home/mvarnold/1-gram_search/')
from mongo_query import Query

import numpy as np
import pandas as pd
from scipy import io
from dateutil.rrule import rrule, MONTHLY
import matplotlib.pyplot as plt
import datetime
import matplotlib.dates as mdates
from multiprocessing import Pool

import argparse


def grab_word(word_i):
    print(word_i)
    try:
        query =  Query('guest', pwd, {'word' : word_i}, word_i)
        data_i = query.query_timeseries(max_range=True)
        count_i, rank_i, freq_i = data_i.values.T
    except AttributeError:
        return
    return freq_i


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f','--filename', help='filename', required=True)
    args = parser.parse_args()
    return args


def main():
    filename_base = parse_args().filename

    with open(f"../data/{filename_base}.txt", 'r') as f:
        words = [line.rstrip() for line in f]

    index = pd.date_range(start=datetime.datetime(2008,9,9),end=datetime.date.today(),freq='D')

    with Pool(30) as p:
        data_list = p.map(grab_word, words)
    
    np.savetxt(f"../data/{filename_base}.dat", np.array(data_list))


if __name__ == "__main__":
    main()
